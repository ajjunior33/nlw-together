```ts
import express from "express";

const app = express();

/**
 * GET    =>  Buscar uma informação.
 * POST   =>  Inserir (Criar) uma informação.
 * PUT    =>  Alteraruma informação.
 * DELETE =>  Remover um dado.
 * PATCH  =>  Alterar uma informação específica.
 */

/**
 * Tipos de parâmetros
 * Routes Params    =>  http://localhost:3000/produtos/1234
 * Query Params     =>  http://localhost:3000/produtos?nama=teclado&description=tecladobom
 *
 * Body Params      =>  {
 *  "name"=>"teclado",
 *  "description" => "Teclado Bom"
 * }
 */
app.get("/test", (request, response) => {
  // Request   => Entrada
  // Response  => Saida

  return response.send("Olá, Mundo!");
});
app.post("/test", (request, response) => {
  return response.send("Olá, Mundo!");
});

app.listen(3000, () => {
  console.log("Server is running.");
});
```
