
# Database SQLITE

```json
{
  "type": "sqlite",
  "database": "src/database/database.sqlite",
  "migrations": ["src/database/migrations/*.ts"],
  "entities": ["src/entities/*.ts"],
  "cli": {
    "migrationsDir": "src/database/migrations",
    "entitiesDir": "src/entities"
  }
}
```

# Melhrias

- Enviar e-mail informando que o usuário recebeu um elogío.
- Colocar em produção.
- Criar front-end.