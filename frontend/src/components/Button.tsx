import { ButtonHTMLAttributes } from "react";

import "../styles/button.components.scss";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

export function Button(props: ButtonProps) {
  return <button className="button" {...props}></button>;
}
